﻿using Autofac;
using Microsoft.EntityFrameworkCore;

var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
optionsBuilder.UseInMemoryDatabase("testDatabase");

var builder = new ContainerBuilder();

builder.Register(c =>
{
    // Tạo options từ DbContextOptionsBuilder
    var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
    optionsBuilder.UseInMemoryDatabase("testDatabase"); // Có thể thay đổi bằng UseSQL...
    return optionsBuilder.Options;
}).As<DbContextOptions<AppDbContext>>();

// Đăng ký AppDbContext
builder.RegisterType<AppDbContext>();

// Tạo Autofac Container từ ContainerBuilder
var container = builder.Build();

// Tự động khởi tạo object có kiểu là AppDbContext
var context = container.Resolve<AppDbContext>();

//var context = new AppDbContext(optionsBuilder.Options);

// tạo danh sách person sẽ thêm vào database
var personData = new List<Person>
{
    new Person
    {
        Name = "A",
    },
    new Person
    {
        Name = "B",
    }
};

// đánh dấu là thêm vào database
await context.Persons.AddRangeAsync(personData);
await context.SaveChangesAsync();

var persons = await context.Persons.ToListAsync();
persons.ForEach(person => Console.WriteLine($"{person.Id} {person.Name}"));



