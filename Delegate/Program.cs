﻿MyDelegate myDelegate = (int x) => Console.WriteLine("This is an anonymous method. The value of x is: " + x);
myDelegate(10);
delegate void MyDelegate(int x);