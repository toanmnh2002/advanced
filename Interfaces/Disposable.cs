﻿namespace Interfaces
{
    public class Disposable : IDisposable
    {
        private bool disposed = false;

        // Constructor
        public Disposable()
        {
            // Khởi tạo và cấp phát tài nguyên không quản lý ở đây
        }

        // Phương thức Dispose để giải phóng tài nguyên
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Giải phóng tài nguyên quản lý ở đây (nếu có)
                }

                // Giải phóng tài nguyên không quản lý ở đây

                disposed = true;
            }
        }

        // Destructor (finalizer)
        ~Disposable()
        {
            Dispose(false);
        }
    }
}
