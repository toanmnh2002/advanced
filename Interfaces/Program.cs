﻿using Interfaces;

List<Person> people = new()
{
    new Person { Age=20,Name="Toan"},
    new Person { Age=21,Name="Nicky"},
    new Person { Age=12,Name="Jack"},
    new Person { Age=12,Name="Abey"},
};
people.Sort();
foreach (var item in people)
{
    Console.WriteLine(item.Name + ":" + item.Age);
}

var PersonA = new Person
{
    Age = 20,
    Name = "Cole"
};
var PersonB = new Person
{
    Age = 20,
    Name = "Thiago"
};
var CompareAge = PersonA.Equals(PersonB);
Console.WriteLine($"Result:{CompareAge}");

people.Sort(new Person());
foreach (var item in people)
{
    Console.WriteLine(item.Name + ":" + item.Age);
}

Console.ReadKey(true);

#region Disposable
// Khai báo và sử dụng một đối tượng Disposable trong một khối using
using (Disposable disposableObj = new Disposable())
{
    // Sử dụng đối tượng disposableObj ở đây
    Console.WriteLine("Using disposableObj...");
} // Khi khối using kết thúc, Dispose() sẽ tự động được gọi

// Đoạn mã tiếp theo sẽ chứa các tài nguyên không quản lý
// Khi Main kết thúc, destructor của Disposable sẽ tự động được gọi (nếu cần)
#endregion





