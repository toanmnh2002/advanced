﻿namespace Interfaces
{
    public class Person : IComparable<Person>, IEquatable<Person>, IComparer<Person>
    {
        public int Age { get; set; }
        public string? Name { get; set; }
        public int CompareTo(Person? other) => Name.CompareTo(other?.Name);
        public bool Equals(Person? other) => Age == other?.Age;
        public int Compare(Person? x, Person? y)
        {
            int result = x.Name.CompareTo(y.Name);
            if (result is 0) result = x.Age.CompareTo(y.Age);
            return result;
        }
    }
}
