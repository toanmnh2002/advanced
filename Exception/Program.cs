﻿//Chuyển đổi chuỗi sang số

//int x = int.Parse("hi");
//Console.WriteLine("Done"); // dòng này sẽ không bao giờ được thực thi.
// Chương trình sẽ kết thúc đột ngột

//Truy cập index không nằm trong mảng
//int[] a = { 1, 1, 1 };
//Console.WriteLine(a[-1]);
//Console.WriteLine("Done");

//A a = null;
//Console.WriteLine(a.K);

//// System.NullReferenceException: 'Object reference not set to an instance of an object.'
////Truy cập vào member của object chưa được khởi tạo và có giá trị là null
//class A
//{
//    public int K { get; set; } = 3000;
//}

//try
//{
//    int.Parse("hi"); // lỗi chỗ này
//    Console.WriteLine("DONE");
//}
//catch (Exception ex) // object ex đại diện cho exception nó chứa các thông tin liên quan
//{
//    Console.WriteLine($"ex.Message: {ex.Message}"); // trả về thông điệp mô tả lỗi.
//    Console.WriteLine($"ex.StackTrace: {ex.StackTrace}"); // trả về chuỗi mô tả vị trí gọi phương thức nơi xảy ra lỗi.
//    Console.WriteLine($"ex.GetType(): {ex.GetType()}"); // trả về kiểu Exception.
//    Console.WriteLine($"ex.ToString(): {ex.ToString()}"); // kết hợp Message và StackTrace.
//}
//Console.WriteLine("END");

//class Program
//{
//    static void Handle()
//    {
//        try
//        {
//            int[] numbers = { 1, 2, 3 };
//            Console.WriteLine(numbers[4]);
//            string str = null;
//            Console.WriteLine(str.Length);
//        }
//        catch (IndexOutOfRangeException ex)
//        {
//            Console.WriteLine($"{ex.Message}");
//        }
//        catch (NullReferenceException ex1)
//        {
//            Console.WriteLine($"{ex1.Message}");
//        }
//        catch (Exception)
//        {
//            Console.WriteLine("Lỗi không xác định.");
//        }
//    }
//}


try
{
    Console.WriteLine("first");
    return;
}
catch (Exception ex)
{
    Console.WriteLine("second");
}
finally
{
    Console.WriteLine("final");
}
Console.WriteLine("end");

//try
//{
//    int.Parse("first");
//    Console.WriteLine("second");
//}
//catch (Exception ex)
//{
//    Console.WriteLine("third");
//    return;
//}
//finally
//{
//    Console.WriteLine("final");
//}
//Console.WriteLine("end");

//class BankAccount
//{
//    public decimal balance { get; set; }

//    public void MakePurchase(decimal amount)
//    {
//        if (amount <= 0)
//        {
//            throw new ArgumentException("Invalid input for amount");
//        }

//        if (amount > balance)
//        {
//            throw new InvalidOperationException("Not enough balance");
//        }
//        balance -= amount;
//    }
//}


//void B() => throw new Exception("Thử throw exception"); // Sảy ra Exception

//void A()
//{
//    Console.WriteLine("A start");
//    try
//    {
//        B();
//    }
//    catch (Exception)
//    {
//        throw; // Chuyển tiếp Exception
//    }
//    Console.WriteLine("A end");
//}
//try
//{
//    Console.WriteLine("try start");
//    A();
//    Console.WriteLine("try end");
//}
//catch (Exception ex)
//{
//    Console.WriteLine("catch");
//}


//try
//{
//    throw new CustomException("Exception is created by ManhToan'.");
//}
//catch (CustomException ex)
//{
//    Console.WriteLine($"Custom Exception Caught: {ex.Message}");
//}
//public class CustomException : Exception
//{

//    public CustomException()
//    {
//    }

//    public CustomException(string message)
//        : base(message)
//    {
//    }

//    public CustomException(string message, Exception innerException)
//        : base(message, innerException)
//    {
//    }
//}

//try
//{
//    Console.OutputEncoding = System.Text.Encoding.UTF8;
//    int[] numbers = { 1, 2, 3 };
//    Console.WriteLine(numbers[5]); // Thử truy cập phần tử không tồn tại
//    string str = null;
//    Console.WriteLine(str.Length); // Thử gọi phương thức trên một chuỗi null
//}
//catch (IndexOutOfRangeException ex) when (ex.Message.Contains("5"))
//{
//    Console.WriteLine("Lỗi: IndexOutOfRangeException, nhưng chỉ khi nó liên quan đến số 5.");
//}
//catch (NullReferenceException ex) when (ex.TargetSite.Name == "get_Length")
//{
//    Console.WriteLine("Lỗi: NullReferenceException, nhưng chỉ khi nó xảy ra khi gọi phương thức Length.");
//}
//catch (Exception ex)
//{
//    Console.WriteLine($"Lỗi: {ex.Message}. Lỗi không xác định.");
//}


/*
 Khi hàm B sảy ra lỗi tại dòng int.Parse("hi"); 
các đoạn code tiếp theo sẽ không được thực hiện
Thay vào đó luồng chương trình sẽ duyệt ngược method call stack 
để tìm khối lệnh catch gần nhất.
Khối lệnh catch gần nhất là khối lệnh gọi A();. 
Thế nên khối lệnh catch này sẽ được thực thi.
 */
//void B()
//{
//    Console.WriteLine("B start");
//    int.Parse("hi"); // Exception sảy ra ở chỗ này
//    Console.WriteLine("B end");
//}

//void A()
//{
//    Console.WriteLine("A start");
//    B();
//    Console.WriteLine("A end");
//}

//try
//{
//    Console.WriteLine("try start");
//    A();
//    Console.WriteLine("try end");
//}
//catch (Exception ex)
//{
//    Console.WriteLine("catch");
//}
//Console.WriteLine("-- end --");

//void B() => int.Parse("hi"); // Exception sảy ra ở chỗ này

//void A()
//{
//    Console.WriteLine("A start");
//    try
//    {
//        B();
//    }
//    catch (Exception ex)
//    {
//        Console.WriteLine("catch A");
//    }
//    Console.WriteLine("A end");
//}

//try
//{
//    Console.WriteLine("try start");
//    A();
//    Console.WriteLine("try end");
//}
//catch (Exception ex)
//{
//    Console.WriteLine("catch");
//}

//Console.WriteLine("-- end --");