﻿namespace Solid
{
    public class InterfaceSegregation
    {
        // Giao diện chứa phương thức cơ bản của máy in
        public interface IPrinter
        {
            void Print();
        }

        // Giao diện chứa phương thức quét cho máy quét
        public interface IScanner
        {
            void Scan();
        }

        // Lớp Printer thực thi giao diện IPrinter
        public class Printer : IPrinter
        {
            public void Print()
            {
                Console.WriteLine("Printing...");
            }
        }
        // Lớp Scanner thực thi giao diện IScanner
        public class Scanner : IScanner
        {
            public void Scan()
            {
                Console.WriteLine("Scanning...");
            }
        }

        // Lớp Photocopier có thể làm được cả 2 chức năng
        public class Photocopier : IPrinter, IScanner
        {
            public void Print()
            {
                Console.WriteLine("Printing...");
            }

            public void Scan()
            {
                Console.WriteLine("Scanning...");
            }
        }
    }
}
