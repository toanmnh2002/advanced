﻿namespace Solid
{
    public class OpenClosePrinciple
    {
        // Lớp quản lý đăng nhập
        class LoginManager
        {
            public void LoginWithUsernameAndPassword(string username, string password) => Console.WriteLine("Logging in with username and password...");
            public void LoginWithQRCode(string qrCode) => Console.WriteLine("Logging in with QR code...");
        }
        interface ILoginMethod // Interface cho các cách đăng nhập
        {
            void Login();
        }

        class UsernamePasswordLogin : ILoginMethod // Lớp con cho đăng nhập bằng tên người dùng và mật khẩu
        {
            public void Login() => Console.WriteLine("Logging in with username and password...");
        }

        class QRCodeLogin : ILoginMethod // Lớp con cho đăng nhập bằng mã QR
        {
            public void Login() => Console.WriteLine("Logging in with QR code...");
        }

        class LoginManager2 // Lớp quản lý đăng nhập
        {
            public void Login(ILoginMethod loginMethod)
            {
                loginMethod.Login();
            }
        }
    }
}
