﻿//Dependency Inversion
using static Solid.DependencyInversion;

IBookReader localFileReader = new LocalFileReader();
BookReaderApp app1 = new BookReaderApp(localFileReader);
app1.ReadBook();

Console.WriteLine();

IBookReader webServiceReader = new WebServiceReader();
BookReaderApp app2 = new BookReaderApp(webServiceReader);
app2.ReadBook();

public class BookReaderApp
{
    private readonly IBookReader bookReader;

    public BookReaderApp(IBookReader _bookReader)
    {
        bookReader = _bookReader;
    }

    public void ReadBook()
    {
        var text = bookReader.ReadBook();
        // render lên màn hình ...
        Console.WriteLine(text);
    }
}

//Liskov

//using static Solid.LiskovSubstitution;

//Rectangle rectangle = new Rectangle();
//rectangle.SetWidth(5);
//rectangle.SetHeight(10);
//Console.WriteLine(rectangle.GetArea()); // Kết quả: 50

//Rectangle square = new Square();
//square.SetWidth(5);
//square.SetHeight(10); // Hành vi không đúng, vì chiều rộng và chiều cao của hình vuông phải bằng nhau
//Console.WriteLine(square.GetArea()); // Kết quả: 25, không như kết quả mong muốn 50

