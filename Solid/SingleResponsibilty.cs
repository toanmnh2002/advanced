﻿namespace Solid
{
    public class SingleResponsibilty
    {
        // Employee chỉ chứa thông tin của nhân viên
        public class Employee
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Salary { get; set; }
        }

        // SalaryCalculator chịu trách nhiệm tính toán lương
        public class SalaryCalculator
        {
            public decimal CalculateSalary(decimal hoursWorked, decimal hourlyRate)
            {
                return hoursWorked * hourlyRate;
            }
        }
    }
}
