﻿namespace Solid
{
    public class DependencyInversion
    {
        public interface IBookReader
        {
            string ReadBook();
        }

        public class LocalFileReader : IBookReader
        {
            public string ReadBook() => "Reading book from local file...";
        }

        public class WebServiceReader : IBookReader
        {
            public string ReadBook() => "Reading book from web service...";
        }
        /*
         * BookReaderApp phụ thuộc vào IBookReader 
         * thế nên có thể dễ dàng thay đổi việc đọc sách 
         * mà không ảnh hưởng tới chương trình.
         */
    }
}
