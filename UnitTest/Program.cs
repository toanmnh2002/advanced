﻿using Autofac;

ContainerBuilder builder = new ContainerBuilder();

// Chú ý register
builder.RegisterType<B>().SingleInstance();
builder.RegisterType<A>().InstancePerLifetimeScope();

IContainer container = builder.Build();

// lifetime scope 1
ILifetimeScope scope1 = container.BeginLifetimeScope();
A a1 = scope1.Resolve<A>();
A a2 = scope1.Resolve<A>();
Console.WriteLine(ReferenceEquals(a1, a2));

// lifetime scope 2
ILifetimeScope scope2 = container.BeginLifetimeScope();
A a3 = scope2.Resolve<A>();
A a4 = scope2.Resolve<A>();
Console.WriteLine(ReferenceEquals(a3, a4));
public class A
{
    private readonly IB _b;

    public A(IB b) => _b = b;

    public string D()
    {
        string result1 = _b.G();
        string result2 = _b.G();
        return result1 + result2;
    }
}
public interface IB
{
    string G();
}

public class B : IB
{
    public string G() => "Hii";
}

//public class A
//{
//    private readonly B _b;

//    public A(B b) => _b = b;

//    public string D()
//    {
//        string result1 = _b.G();
//        string result2 = _b.G();
//        return result1 + result2;
//    }
//}

//public class B
//{
//    // phải gắn method mock với từ khóa virtual
//    public virtual string G() => "Hii";

//}