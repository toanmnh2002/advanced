﻿//Toán tử ?? (null-coalescing operator):

/*Toán tử ?? được sử dụng trong biểu thức để trả về giá trị mặc định nếu biểu thức trước nó có giá trị là null.
Cú pháp: expression1 ?? expression2
Nếu expression1 không null, giá trị của expression1 được trả về. 
Nếu expression1 là null, giá trị của expression2 được trả về.
Ví dụ:
*/
int? nullableInt = null;
int result = nullableInt ?? 10;
Console.WriteLine(result);

//Toán tử ??= (null-coalescing assignment operator):

/*Toán tử ??= được sử dụng để gán giá trị cho một biến chỉ khi biến đó có giá trị là null.
Cú pháp: variable ??= expression
Nếu biến variable không null, không có gán giá trị nào được thực hiện. 
Nếu biến variable là null, giá trị của expression sẽ được gán cho variable.*/

int result1 = nullableInt ??= 10;
Console.WriteLine(result1);