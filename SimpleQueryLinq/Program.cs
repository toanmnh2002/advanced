﻿// Hãy giả định chúng ta có một danh sách các sản phẩm của một cửa hàng với các thuộc tính sau: 
// tên sản phẩm, giá, và danh mục (category). 
// Hãy tạo một danh sách các sản phẩm và thực hiện các yêu cầu sau bằng cách sử dụng Linq
Console.OutputEncoding = System.Text.Encoding.UTF8;
// Tạo danh sách các sản phẩm:
var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm dạ hội", Price = 1500000, Category = "Đầm" },
    // Thêm các sản phẩm khác tại đây...
};

// Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:
var listProduct = products
    .Where(product => product.Price > 500)
    .ToList();
//foreach (var item in listProduct)
//{
//    Console.WriteLine($"{item.Name},{item.Price},{item.Category}");
//}
// Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:
var productsPrice = products
    .Where(product => product.Price < 300000)
    .OrderByDescending(product => product.Price)
    .Select(product => product.Name);

foreach (var item in listProduct)
{
    Console.WriteLine($"{item.Name}");
}
// Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:
var categoryDictionary = products
    .GroupBy(product => product.Category)
    .ToDictionary(group => group.Key, group => group.ToList());

foreach (var category in categoryDictionary)
{
    Console.WriteLine($"Danh mục: {category.Key}");
    foreach (var product in category.Value)
    {
        Console.WriteLine($"Tên sản phẩm: {product.Name}, Giá: {product.Price}");
    }
}

// Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":
var totalPriceOfShirts = products
    .Where(product => product.Category == "Áo")
    .Sum(product => product.Price);

Console.WriteLine($"Tổng tiền:{totalPriceOfShirts}");
// Tìm sản phẩm có giá cao nhất:
var mostExpensiveProduct = products
    .OrderByDescending(product => product.Price)
    .FirstOrDefault();
//Console.WriteLine($"Sản phẩm có giá cao nhất là {mostExpensiveProduct}");

// Tạo danh sách các sản phẩm với tên đã viết hoa:
var productsWithUpperCaseNames = products
    .Select(product =>
    new Product
    {
        Name = product.Name.ToUpper(),
        Price = product.Price,
        Category = product.Category
    });
//productsWithUpperCaseNames.ToList().ForEach(product => Console.WriteLine($"Tên:{product.Name},Giá:{product.Price},Loại:{product.Category}"));

class Product
{
    public string Name { get; set; }
    public int Price { get; set; }
    public string Category { get; set; }
}