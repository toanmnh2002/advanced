﻿// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;

var productsJson = File.ReadAllText("products.json");
var categoriesJson = File.ReadAllText("categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);


//Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.
//var producta = products?
//    .Where(p => p.Price > 100)
//    .OrderByDescending(p => p.Price)
//    .Select(p => new { p.Name, p.Price });
//producta?.ToList().ForEach(item => Console.WriteLine($"{item.Name}-{item.Price}"));
//Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var categoryCounts = categories
    .GroupJoin(products,
                category => category.Id,
                product => product.CategoryId,
                (category, productList) => new
                {
                    CategoryName = category.Name,
                    ProductCount = productList.Count()
                })
    .OrderBy(categoryCount => categoryCount.CategoryName);

//categoryCounts?.ToList().ForEach(item => Console.WriteLine($"{item.CategoryName}-{item.ProductCount}"));



//Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var priceAverage = categories
    .GroupJoin(products,
                category => category.Id,
                product => product.CategoryId,
                (category, productList) => new
                {
                    CategoryName = category.Name,
                    AveragePrice = productList.Any() ? Math.Round(productList.Average(x => x.Price), 2) : 0
                })
    .OrderBy(x => x.CategoryName);
//priceAverage?.ToList().ForEach(item => Console.WriteLine($"{item.CategoryName}-{item.AveragePrice}"));

//Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.
var topExpensiveProducts = products
    .OrderByDescending(product => product.Price)
    .Take(10)
    .Select(product => new { product.Name, product.Price, product.Id });

//topExpensiveProducts?.ToList().ForEach(item => Console.WriteLine($"{item.Id}-{item.Name}-{item.Price}"));

//Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.
var averagePricesByCategoryDescending = categories
    .GroupJoin(products,
                category => category.Id,
                product => product.CategoryId,
                (category, productList) => new
                {
                    CategoryName = category.Name,
                    AveragePrice = productList.Any() ? Math.Round(productList.Average(product => product.Price), 2) : 0
                })
    .OrderByDescending(result => result.AveragePrice);

//averagePricesByCategoryDescending?.ToList().ForEach(item => Console.WriteLine($"Category: {item.CategoryName}, Average Price: {item.AveragePrice}"));

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.

var productsUnder50 = products
    .Where(product => product.Price < 50)
    .OrderBy(product => product.Name)
    .Select(product => new
    {
        ProductName = product.Name,
        CategoryName = categories.FirstOrDefault(category => category.Id == product.CategoryId)?.Name
    });
//productsUnder50?.ToList().ForEach(item => Console.WriteLine($"Product Name: {item.ProductName}, Category: {item.CategoryName}"));

//Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var totalPricesByCategory = categories
    .GroupJoin(products,
                category => category.Id,
                product => product.CategoryId,
                (category, productList) => new
                {
                    CategoryName = category.Name,
                    TotalPrice = productList.Sum(product => product.Price)
                })
    .OrderBy(result => result.CategoryName);

//totalPricesByCategory?.ToList().ForEach(item => Console.WriteLine($"Category: {item.CategoryName}, Total Price: {item.TotalPrice}"));

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần.
var appleProducts = products
    .Where(product => product.Name.Contains("Apple"))
    .OrderBy(product => product.Name)
    .Select(product => new
    {
        ProductName = product.Name,
        CategoryName = categories.FirstOrDefault(category => category.Id == product.CategoryId)?.Name
    });

//appleProducts?.ToList().ForEach(item => Console.WriteLine($"Product Name: {item.ProductName}, Category: {item.CategoryName}"));

//Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.
var totalPricesByCategoryGreaterThan1000 = categories
    .GroupJoin(products,
                category => category.Id,
                product => product.CategoryId,
                (category, productList) => new
                {
                    CategoryName = category.Name,
                    TotalPrice = productList.Sum(product => product.Price)
                })
        .Where(result => result.TotalPrice > 1000)
        .OrderBy(result => result.CategoryName);
//totalPricesByCategoryGreaterThan1000?.ToList().ForEach(item => Console.WriteLine($"Category: {item.CategoryName}, Total Price: {item.TotalPrice}"));

//Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó.
var productsPriceLowerthan10 = products
    .Where(product => product.Price < 10)
    .OrderBy(product => product.Name)
    .Select(product => new
    {
        ProductName = product.Name,
        CategoryName = categories.FirstOrDefault(category => category.Id == product.CategoryId)?.Name
    });
//productsPriceLowerthan10?.ToList().ForEach(item => Console.WriteLine($"ProductName: {item.CategoryName}, CategoryName: {item.CategoryName}"));

//Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var maxPriceProductsByCategory = categories
    .GroupJoin(products,
                category => category.Id,
                product => product.CategoryId,
                (category, productList) => new
                {
                    CategoryName = category.Name,
                    MaxPriceProduct = productList.OrderByDescending(p => p.Price).FirstOrDefault()
                })
    .Select(x => new
    {
        CategoryName = x.CategoryName,
        ProductName = x.MaxPriceProduct != null ? x.MaxPriceProduct.Name : "",
        MaxPrice = x.MaxPriceProduct != null ? x.MaxPriceProduct.Price : 0
    })
    .OrderBy(result => result.CategoryName);

//maxPriceProductsByCategory?.ToList().ForEach(item => Console.WriteLine($"CategoryName: {item.CategoryName}, ProductName {item.ProductName},CategoryName: {item.MaxPrice}"));

//Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.
var sumProductsByCategory = categories
    .GroupJoin(products,
                category => category.Id,
                product => product.CategoryId,
                (category, productList) => new
                {
                    CategoryName = category.Name,
                    ProductName = productList.FirstOrDefault(x => x.Id == category.Id)?.Name,
                    TotalPrice = productList.Sum(p => p.Price)
                })
    .OrderByDescending(x => x.TotalPrice);
//sumProductsByCategory?.ToList().ForEach(item => Console.WriteLine($"CategoryName: {item.CategoryName}, ProductName {item.ProductName},Sum: {item.TotalPrice}"));

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.
decimal averagePrice = products.Average(p => p.Price);
var productsByCategory = categories
    .GroupJoin(products,
                category => category.Id,
                product => product.CategoryId,
                (category, productList) => new
                {
                    CategoryName = category.Name,
                    Products = productList.Where(x => x.Price > averagePrice)
                })
    .SelectMany(x => x.Products.Select(p => new
    {
        CategoryName = x.CategoryName,
        ProductName = p.Name
    }));
//productsByCategory?.ToList().ForEach(item => Console.WriteLine($"CategoryName: {item.CategoryName}, ProductName: {item.ProductName}"));

//Tính tổng số tiền của tất cả các sản phẩm.
decimal totalAmount = products.Sum(p => p.Price);
//Console.Write($"Sum of All produdcts:{totalAmount}");

//Lấy danh sách các danh mục và số lượng sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo số lượng sản phẩm giảm dần.

var maxPriceProductsCategory = products
    .GroupBy(p => p.CategoryId)
    .Select(x => new
    {
        CategoryId = x.Key,
        ProductCount = x.Count(),
        MaxPriceProduct = x.OrderByDescending(p => p.Price).FirstOrDefault()
    })
    .OrderByDescending(item => item.ProductCount);
//maxPriceProductsCategory?.ToList().ForEach(item => Console.WriteLine($"CategoryName: {item.CategoryId}, ProductName:{item.MaxPriceProduct?.Name},Quantity: {item.ProductCount}, Price: {item.MaxPriceProduct?.Price}"));
//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị giảm dần.
var minPriceProductsCategory = products
    .GroupBy(p => p.CategoryId)
    .Select(x => new
    {
        CategoryId = x.Key,
        ProductCount = x.Count(),
        MinPriceProduct = x.OrderBy(p => p.Price).FirstOrDefault()
    })
    .OrderByDescending(item => item.MinPriceProduct?.Price)
    .ToList();

//minPriceProductsCategory?.ForEach(item => Console.WriteLine($"CategoryName: {item.CategoryId}, ProductName:{item.MinPriceProduct?.Name}, Quantity: {item.ProductCount}, Price: {item.MinPriceProduct?.Price}"));

//Lấy danh sách các danh mục có ít sản phẩm nhất.
var categoriesWithMinProducts = categories
    .Select(category => new
    {
        Category = category,
        ProductCount = products.Count(product => product.CategoryId == category.Id)
    })
    .OrderBy(item => item.ProductCount)
    .FirstOrDefault();
//Console.WriteLine($"Category ID: {categoriesWithMinProducts?.Category.Id}, Category Name: {categoriesWithMinProducts?.Category.Name}, Product Count: {categoriesWithMinProducts?.ProductCount}");


//Tính tổng số tiền của các sản phẩm có giá trị lớn hơn $50.
decimal totalPrice = products
    .Where(product => product.Price > 50)
    .Sum(product => product.Price);
//Console.WriteLine($"Total price of products with price greater than $50: {totalPrice}");

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.
var minPriceProductsCategories = products
    .GroupBy(p => p.CategoryId)
    .Select(x => new
    {
        CategoryId = x.Key,
        ProductCount = x.Count(),
        MinPriceProduct = x.OrderBy(p => p.Price).FirstOrDefault()
    })
    .OrderBy(item => item.MinPriceProduct?.Price)
    .ToList();

minPriceProductsCategories?.ForEach(item => Console.WriteLine($"CategoryName: {item.CategoryId}, ProductName:{item.MinPriceProduct?.Name}, Quantity: {item.ProductCount}, Price: {item.MinPriceProduct?.Price}"));






