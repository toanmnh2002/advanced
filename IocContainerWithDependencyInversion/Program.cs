﻿using Autofac;

ContainerBuilder builder = new ContainerBuilder();

// Đăng ký lớp B và chỉ định là implement của interface IB
builder.RegisterType<B>().As<IB>().InstancePerDependency();
builder.RegisterType<A>().InstancePerLifetimeScope();

IContainer container = builder.Build();

// lifetime scope 1
ILifetimeScope scope1 = container.BeginLifetimeScope();
A a1 = scope1.Resolve<A>();
A a2 = scope1.Resolve<A>();
Console.WriteLine(ReferenceEquals(a1, a2));
Console.WriteLine(a1.D());
Console.WriteLine(a2.D());


// lifetime scope 2
ILifetimeScope scope2 = container.BeginLifetimeScope();
A a3 = scope2.Resolve<A>();
A a4 = scope2.Resolve<A>();
Console.WriteLine(ReferenceEquals(a3, a4));

// Thử call a4
Console.WriteLine(a3.D());
Console.WriteLine(a4.D());


public class A
{
    private readonly IB _b;

    public A(IB b) => _b = b;

    public string D()
    {
        string result1 = _b.G();
        string result2 = _b.G();
        return result1 + result2;
    }
}
public interface IB
{
    string G();
}

public class B : IB
{
    public string G() => "Hii";
}