﻿//Generic Class
//var o = new A<string>();
//o.a = "1000";
//Console.WriteLine(o.a);
//public class A<T>
//{
//    public T a { get; set; }
//}

//Generic Method
A a = new A();
a.Message<string>("hello", "toan");
class A
{
    public void Message<T>(T a, T b)
    {
        Console.WriteLine(a);
        Console.WriteLine(b);
    }
}

//Constrain type
//◉ Là một ràng buộc chỉ cho phép kiểu T là kiểu class.
//class A<T> where T : class
//{
//    public T a;
//}

//◉ T bắt buộc phải là kiểu B hoặc là lớp kế thừa B.

//A<B> b = new A<B>();
//A<C> c = new A<C>();
//public class A<T> where T : B
//{ }
//public class B
//{ }
//public class C : B
//{ }

/*Constructor constraint
◉ Kiểu dữ liệu generic phải có một
constructor không tham số.*/
//public class MyClass<T> where T : new()
//{
//    // ...
//}

//A<B> b = new A<B>();
//class A<T> where T : new()
//{
//    // ...
//}
//class B
//{
//    //public B()
//    //{
//    //}

//    public B(int a)
//    { }
//}