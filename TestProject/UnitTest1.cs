﻿using Moq;

public class ATests
{
    [Fact]
    public void D_Returns_Correct_Result()
    {
        // Arrange
        string expectedResult = "KakaKaka";
        // Tạo một mock object của lớp B
        Mock<IB> mockB = new Mock<IB>();
        // Thiết lập hành vi mong muốn cho phương thức G
        mockB.Setup(b => b.G()).Returns("Kaka");
        // Tạo một đối tượng A với mock object của lớp B
        A a = new A(mockB.Object);

        // Act
        string result = a.D();

        // Assert
        Assert.Equal(expectedResult, result);
        // Verify số lần gọi của phương thức G
        mockB.Verify(b => b.G(), Times.Exactly(2));
    }
}