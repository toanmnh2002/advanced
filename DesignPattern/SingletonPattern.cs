﻿namespace DesignPattern
{
    public static class SingletonPattern
    {
        //1
        //public class A
        //{
        //    private static A _instance;
        //    private A() => Console.WriteLine("A is called");
        //    public static A Instance => _instance ??= new A();
        //}

        //public class A
        //{
        //    private static A _instance;
        //    private static readonly object lockObject = new object();
        //    private A() => Console.WriteLine("A is called");
        //    public static A Instance
        //    {
        //        get
        //        {
        //            lock (lockObject)
        //            {
        //                return _instance ??= new A();
        //            }
        //        }
        //    }
        //}
        public class A
        {
            private readonly static Lazy<A> lazy = new Lazy<A>(() => new A());
            private A() => Console.WriteLine("A is called");
            public static A Instance => lazy.Value;
        }
    }
}
