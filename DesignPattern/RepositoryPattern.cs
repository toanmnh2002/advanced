﻿namespace DesignPattern
{
    public class RepositoryPattern
    {
    }
    interface IRepository
    {
        public int GetData(int p);
        public void UpdateData(int s);
    }

    class LocalFileRepository : IRepository
    {
        public int GetData(int p) => p * 2;

        public void UpdateData(int s) => Console.WriteLine($"{s} đã được lưu vào local");
    }
    class RemoteFileRepository : IRepository
    {
        public int GetData(int p) => p * 3000; // đổi logic truy cập dữ liệu

        public void UpdateData(int s) => Console.WriteLine($"{s} lưu vào remote thành công");
        // đổi logic lưu trữ dữ liệu

    }
    class BusineesLogic
    {
        public void Calulation(IRepository _repository)
        {
            int data1 = _repository.GetData(1);
            int data2 = _repository.GetData(2);
            // Giả lập việc tính logic
            int sum = data1 + data2;
            _repository.UpdateData(sum);
        }
    }
}
