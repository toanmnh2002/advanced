﻿//Singleton Pattern
//--1 
//SingletonPattern.A a = SingletonPattern.A.Instance;
//SingletonPattern.A b = SingletonPattern.A.Instance;
//Console.WriteLine(ReferenceEquals(a, b));


//Dependency Injection
//using DesignPattern;

//var car = new DependencyInjection.Car();
//var persons = new DependencyInjection.Person
//{
//    car = car
//};
//persons.Move(car);

using DesignPattern;

Console.OutputEncoding = System.Text.Encoding.UTF8;
BusineesLogic busineesLogic = new BusineesLogic();
IRepository localRepository = new LocalFileRepository();

busineesLogic.Calulation(localRepository);