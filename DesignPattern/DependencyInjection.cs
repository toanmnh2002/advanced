﻿namespace DesignPattern
{
    public static class DependencyInjection
    {
        //Dependency Injection

        public class Person
        {
            //private readonly Car _car;

            //public Person(){}

            //public Person(Car car){_car = car;}
            public Car car { get; set; }
            public void Move(Car car)
            {
                //string result = _car.Drive(); constructor injection
                string result = car.Drive(); //method injection phụ thuộc vào B để thực hiện
                Console.WriteLine(result);
            }
        }

        public class Car
        {
            public string Drive() => "Thanh Cong";
        }
    }
}
