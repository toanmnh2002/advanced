﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Text;

//Seeddata.Seed();
Console.OutputEncoding = Encoding.UTF8;
var context = new AppDbContext();

Person person = new Person
{
    Name = "testAdd"
};

#region Add
EntityEntry<Person> entityentry = context.Entry(person);

Console.WriteLine($"trước khi add {entityentry.State}");

context.Persons.Add(person); // đánh dấu là thêm vào database
Console.WriteLine($"sau khi add {entityentry.State}");

context.SaveChanges(); // lưu mọi sự thay đổi
Console.WriteLine($"sau khi savechanges {entityentry.State}");
#endregion

#region Update
var personToUpdate = context.Persons.FirstOrDefault(x => x.Id == 25); // lấy ra 1 chiếc xe

EntityEntry<Person> entityEntryForUpdate = context.Entry(personToUpdate);

Console.WriteLine($"trước khi update {entityEntryForUpdate.State}");

personToUpdate.Name = "Tesla"; // chỉnh sửa object
Console.WriteLine($"sau khi chỉnh sửa {entityEntryForUpdate.State}");

context.Persons.Update(personToUpdate);
Console.WriteLine($"sau khi gọi hàm update {entityEntryForUpdate.State}");

context.SaveChanges(); // lưu mọi sự thay đổi
Console.WriteLine($"sau khi saveChanges {entityEntryForUpdate.State}");
#endregion

#region Delete
//var personToDelete = context.Persons.FirstOrDefault(x => x.Id == 23); // lấy ra 1 chiếc xe
//var entityEntryDelete = context.Entry(personToDelete);
//context.Persons.Remove(personToDelete); // đánh dấu là sẽ xóa khỏi database
//Console.WriteLine($"sau khi gọi hàm Remove {entityEntryDelete.State}");

//context.SaveChanges(); // lưu mọi sự thay đổi
//Console.WriteLine($"sau khi saveChanges {entityEntryDelete.State}");
#endregion

#region Query
//Person personToQuery = context.Persons.FirstOrDefault(x => x.Id == 16);
//EntityEntry<Person> entityEntry = context.Entry(personToQuery);
//Console.WriteLine($"sau khi query xong {entityEntry.State}");

//personToQuery.Name = "hahsdaha"; // không hề gọi hàm update
//Console.WriteLine($"sau khi thay đổi giá trị {entityEntry.State}");

//int count = context.SaveChanges();
//Console.WriteLine($"sau khi SaveChanges {entityEntry.State}");
//Console.WriteLine(count);

#endregion

#region Tracking set full detach
var persontrack = context.Persons.AsNoTracking().FirstOrDefault(x => x.Id == 1); // lấy ra 1 chiếc xe
EntityEntry<Person> entityEntry = context.Entry(persontrack);
Console.WriteLine($"sau khi query xong {entityEntry.State}");

persontrack.Name = "32";
Console.WriteLine($"sau khi sửa xong {entityEntry.State}");

context.Persons.Update(person);
Console.WriteLine($"sau khi update xong {entityEntry.State}");

context.SaveChanges(); // dữ liệu sẽ không được lưu vào dbchange
Console.WriteLine($"sau khi SaveChanges xong {entityEntry.State}");
#endregion