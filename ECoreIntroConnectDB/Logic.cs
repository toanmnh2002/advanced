﻿using Microsoft.EntityFrameworkCore;

namespace ECoreIntroConnectDB
{
    public class Logic(AppDbContext appDbContext)
    {
        private readonly AppDbContext _appDbContext = appDbContext;
        public async Task Create(Person person)
        {
            await _appDbContext.AddAsync(person);
            await _appDbContext.SaveChangesAsync();
        }
        public async void Update(Person person)
        {
            _appDbContext.Update(person);
            await _appDbContext.SaveChangesAsync();
        }

        public async void Delete(Person person)
        {
            _appDbContext.Remove(person);
            await _appDbContext.SaveChangesAsync();
        }

        public async void Print() => await _appDbContext.Persons.ToListAsync();

    }
}
