﻿using Microsoft.EntityFrameworkCore;

namespace ECoreIntroConnectDB
{
    public static class Seeddata
    {
        public static async void Seed()
        {
            var context = new AppDbContext();
            var personData = new List<Person>
            {
                new Person { Name = "ToanNguyen", },
                new Person { Name = "ToanManhNguyen", }
            };

            await context.Persons.AddRangeAsync(personData);
            await context.SaveChangesAsync();

            var persons = await context.Persons.ToListAsync();
            persons.ForEach(x => Console.WriteLine($"PersonId:{x.Id}-PersonName:{x.Name}"));
        }
    }
}
