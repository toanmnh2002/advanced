﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<Person> Persons { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //optionsBuilder.UseInMemoryDatabase("testDatabase");
        optionsBuilder.UseSqlServer("Server=(local);Uid=sa;Pwd=123;Database=EfCoreIntro;TrustServerCertificate=True");
        //optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=e1cardiodev;User Id=postgres;Password=123");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Person>().HasData(
            new Person
            {
                Id = 1,
                Name = "ToanNguyenn",
            });
        base.OnModelCreating(modelBuilder);
    }
}


