﻿/*Sử dụng để đánh dấu các thành phần của class (như property, method hoặc class)
là tĩnh(static).
Những phần được đánh dấu là tĩnh(static)
không thuộc instance(object) của class mà thuộc về class.*/

//static method & static property thuộc về class và truy cập trực tiếp thông qua class.

//Truy cập static member gián tiếp

A a = new A();
Console.WriteLine(a.K);
Console.WriteLine(A.z); // Truy cập trực tiếp từ class
A.Hi(); // Truy cập trực tiếp từ class
a.Hello();
class A
{
    public int K => z;
    public void Hello() => Hi();
    public static int z { get; set; } = 3000; // static property
    public static void Hi() => Console.WriteLine("hi");// static method
}

//class A
//{
//    public int F { get; set; } = 3000; // thuộc về object
//    public static void Hi() // thuộc về class
//=> Console.WriteLine(F); // Có thể truy cập được không?

//}
//static member không thể truy cập vào non-static member.
//Vì static member thuộc về class nó không phụ thuộc vào bất kỳ đối tượng cụ thể nào.




