﻿using Microsoft.EntityFrameworkCore;

public partial class AppDbContext : DbContext
{
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Address> Addresss { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //optionsBuilder.UseInMemoryDatabase("testDatabase");
        optionsBuilder.UseSqlServer("Server=(local);Uid=sa;Pwd=123;Database=Migration;TrustServerCertificate=True");
        //optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=e1cardiodev;User Id=postgres;Password=123");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Customer>().HasOne(c => c.Address).WithOne(ad => ad.Customer).HasForeignKey<Address>(ad => ad.CustomerId);
        modelBuilder.Entity<Customer>()
            .HasMany(c => c.Orders)
            .WithOne(o => o.Customer)
            .HasForeignKey(o => o.CustomerId);
        modelBuilder.Entity<OrderDetail>()
                    .HasOne(od => od.Order)
                    .WithMany(o => o.OrderDetails)
                    .HasForeignKey(od => od.OrderId);

        modelBuilder.Entity<OrderDetail>()
                    .HasOne(od => od.Product)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(od => od.OrderId);

        modelBuilder.Entity<OrderDetail>().HasKey(sc => new { sc.OrderId, sc.ProductId });

    }

}