﻿public partial class AppDbContext
{
    public class Product
    {
        public int Id { get; set; }
        public int ProductName { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }

}
