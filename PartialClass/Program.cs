﻿////partial method là phương thức được chia thành hai phần: 
///phần khai báo & phần triển khai.
////Phần khai báo của partial method tồn tại trong một file 
///nhưng phần triển khai có thể được đặt trong file khác.


A a = new A();
a.K();

partial class A // file A1.cs
{
    public partial void K();
}

partial class A // file A2.cs
{
    public partial void K() => Console.WriteLine("partial method");
}
