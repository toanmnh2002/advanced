﻿using Microsoft.EntityFrameworkCore;

namespace GenericRepo
{
    public class AppDBContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlServer("Server=(local);Uid=sa;Pwd=123;Database=SampleEFCore;TrustServerCertificate=True");
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Student>()
                .HasData(
                new Student
                {
                    Id = 1,
                    FullName = "Toan"
                }
                );
        }
    }
}
