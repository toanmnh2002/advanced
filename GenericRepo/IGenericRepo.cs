﻿using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace GenericRepo
{
    public interface IGenericRepo<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task AddEntityAsync(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);
        Task AddRangeAsync(IEnumerable<TEntity> entities);
        void UpdateRange(IEnumerable<TEntity> entities);
        void RemoveRange(IEnumerable<TEntity> entities);
        Task<TEntity?> GetEntityByGenericAsync(Expression<Func<TEntity, bool>> expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null);
    }
}