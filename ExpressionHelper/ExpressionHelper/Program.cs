﻿// Tạo một biểu thức lambda để lọc các Person có tuổi lớn hơn hoặc bằng 18
using System.Linq.Expressions;


Expression<Func<Person, bool>> agePredicate = ExpressionHelper.BuildAgePredicate(18);

// Sử dụng biểu thức lambda để lọc danh sách các Person
var people = new[]
{
    new Person { Name = "Alice", Age = 20 },
    new Person { Name = "Bob", Age = 25 },
    new Person { Name = "Charlie", Age = 15 }
};

var adults = people.Where(agePredicate.Compile());

foreach (var person in adults)
{
    Console.WriteLine($"{person.Name} is an adult.");
}

#region SwitchcaseExpression
//Console.Write("Nhập lựa chọn: ");
//int.TryParse(Console.ReadLine(), out int operation);
//int result = operation switch
//{
//    1 => 2,
//    2 => 3,
//    3 => 4,
//    4 => 5,
//};
//Console.WriteLine(result);
#endregion

public class Person
{
    public string Name { get; set; }
    public int Age { get; set; }
}