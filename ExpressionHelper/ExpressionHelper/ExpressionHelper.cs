﻿using System.Linq.Expressions;

public class ExpressionHelper
{
    public static Expression<Func<Person, bool>> BuildAgePredicate(int minAge)
    {
        ParameterExpression parameter = Expression.Parameter(typeof(Person), "person");
        MemberExpression member = Expression.PropertyOrField(parameter, "Age");
        ConstantExpression constant = Expression.Constant(minAge, typeof(int));
        BinaryExpression body = Expression.GreaterThanOrEqual(member, constant);

        return Expression.Lambda<Func<Person, bool>>(body, parameter);
    }
}